<?php


namespace App\Controller;


use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;
use KnpU\OAuth2ClientBundle\Client\ClientRegistry;
use KnpU\OAuth2ClientBundle\Client\Provider\FacebookClient;
use KnpU\OAuth2ClientBundle\Client\Provider\GithubClient;
use Symfony\Component\HttpFoundation\RedirectResponse;

class SecurityController extends AbstractController
{

    #[Route(path: '/api/login', name: 'api_login', methods: ['POST'])]
    public function apiLogin() {
        $user = $this->getUser();
        return $this->json([
            'username' => $user->getUsername(),
            'roles' => $user->getRoles()
        ]);
    }

    /**
     * @Route("/logout", name="app_logout")
     */
    public function logout()
    {
        throw new \Exception('should not be reached');
        //throw new \LogicException('This method can be blank - it will be intercepted by the logout key on your firewall.');
    }

    /**
     * @Route("/login", name="app_login")
     */
    public function login(AuthenticationUtils $authenticationUtils): Response
    {
        // if ($this->getUser()) {
        //     return $this->redirectToRoute('target_path');
        // }

        // get the login error if there is one
        $error = $authenticationUtils->getLastAuthenticationError();
        // last username entered by the user
        $lastUsername = $authenticationUtils->getLastUsername();

        return $this->render('security/login.html.twig', ['last_username' => $lastUsername, 'error' => $error]);
    }

    /**
     * @Route("/connect/github", name="github_connect")
     * @param ClientRegistry $clientRegistry
     * @return RedirectResponse
     */
    public function connectGithub(ClientRegistry $clientRegistry): RedirectResponse
    {
        /** @var GithubClient $client*/
        $client = $clientRegistry->getClient('github');
        return $client->redirect(['read:user','user:email']);
    }
    /**
     * @Route("/connect/facebook", name="facebook_connect")
     * @param ClientRegistry $clientRegistry
     * @return RedirectResponse
     */
    public function connectFacebook(ClientRegistry $clientRegistry): RedirectResponse
    {
        /** @var FacebookClient $client*/
        $client = $clientRegistry->getClient('facebook');
        return $client->redirect(['public_profile', 'email']);
    }
    /**
     * @Route("/connect/google", name="google_connect")
     * @param ClientRegistry $clientRegistry
     * @return RedirectResponse
     */
    public function connectGoogle(ClientRegistry $clientRegistry): RedirectResponse
    {
        /** @var GithubClient $client*/
        $client = $clientRegistry->getClient('google');
        return $client->redirect(['profile', 'email']);
    }

}
