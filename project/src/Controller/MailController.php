<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Mime\Email;

use Symfony\Component\Mailer\MailerInterface;

class MailController extends AbstractController
{
    /**
     * @Route("/mail", name="mail")
     */
    public function index(MailerInterface $mailer): Response
    {
        $email = (new Email())
                ->from('from@example.fr')
                ->to('to@example.fr')
                ->subject('Test mail dev')
                ->text('ceci est un texte');

        $mailer->send($email);

        return $this->render('mail/index.html.twig');
    }
}
