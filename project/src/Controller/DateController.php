<?php
namespace App\Controller;

use App\Helpers\DateHelper;
use App\Repository\UserRepository;
use Symfony\Component\Security\Core\Security;

class DateController
{
    public function __construct(private Security $security)
    {
    }

    public function __invoke(string $id, UserRepository $userRepository)
    {
        $dateHelper = new DateHelper();
        return $dateHelper->getNextDropDate();
    }
}