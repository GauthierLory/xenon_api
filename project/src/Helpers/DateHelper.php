<?php


namespace App\Helpers;


class DateHelper
{

    const WEEK_DAYS = [0,5,6];

    public function __construct()
    {
        setlocale(LC_TIME, 'fr_FR','fra');
        date_default_timezone_set('Europe/Paris');
    }

    public function getNextDropDate(){
        $dayNumber = date('N');
        if(in_array($dayNumber,self::WEEK_DAYS)){
            switch ($dayNumber){
                case 5:
                    $dates = [[
                    'date_name' => utf8_encode(strftime('%A %d %B %Y',strtotime('now'))),
                    'date_time' => date('Y-m-d',strtotime('now'))
                    ],[
                    'date_name' => utf8_encode(strftime('%A %d %B %Y',strtotime('next Saturday'))),
                    'date_time' => date('Y-m-d',strtotime('next Saturday'))
                    ],[
                    'date_name' => utf8_encode(strftime('%A %d %B %Y',strtotime('next Monday'))),
                    'date_time' => date('Y-m-d',strtotime('next Monday'))
                    ]];
                    break;
                case 6:
                    $dates = [[
                    'date_name' => utf8_encode(strftime('%A %d %B %Y',strtotime('now'))),
                    'date_time' => date('Y-m-d',strtotime('now'))
                    ],[
                    'date_name' => utf8_encode(strftime('%A %d %B %Y',strtotime('next Monday'))),
                    'date_time' => date('Y-m-d',strtotime('next Monday'))
                    ]];
                    break;
                case 0:
                    $dates = [[
                    'date_name' => utf8_encode(strftime('%A %d %B %Y',strtotime('next Friday'))),
                    'date_time' => date('Y-m-d',strtotime('next Friday'))
                    ],[
                    'date_name' => utf8_encode(strftime('%A %d %B %Y',strtotime('next Saturday'))),
                    'date_time' => date('Y-m-d',strtotime('next Saturday'))
                    ],[
                    'date_name' => utf8_encode(strftime('%A %d %B %Y',strtotime('+8 days'))),
                    'date_time' => date('Y-m-d',strtotime('+8 days'))
                    ]];
                break;
            }
        }else{
            $dates = [[
            'date_name' => utf8_encode(strftime('%A %d %B %Y',strtotime('next Friday'))),
            'date_time' => date('Y-m-d',strtotime('next Friday'))
            ],[
            'date_name' => utf8_encode(strftime('%A %d %B %Y',strtotime('next Saturday'))),
            'date_time' => date('Y-m-d',strtotime('next Saturday'))
            ],[
            'date_name' => utf8_encode(strftime('%A %d %B %Y',strtotime('next Monday'))),
            'date_time' => date('Y-m-d',strtotime('next Monday'))
            ]];
        }
        return $dates;
    }
    public function getRangeDates($start_date,$end_date){
        $dates = array();
        $period = new DatePeriod(
        new DateTime($start_date),
        new DateInterval('P1D'),
        new DateTime($end_date)
        );

        foreach ($period as $key => $value) {
            array_push($dates,$value->format('Y-m-d'));
        }
        return $dates;
    }
}