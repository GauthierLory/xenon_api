<?php

namespace App\Repository;

use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\Security\Core\Exception\UnsupportedUserException;
use Symfony\Component\Security\Core\User\PasswordAuthenticatedUserInterface;
use Symfony\Component\Security\Core\User\PasswordUpgraderInterface;
use League\OAuth2\Client\Provider\GithubResourceOwner;
use League\OAuth2\Client\Provider\ResourceOwnerInterface;

/**
 * @method User|null find($id, $lockMode = null, $lockVersion = null)
 * @method User|null findOneBy(array $criteria, array $orderBy = null)
 * @method User[]    findAll()
 * @method User[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UserRepository extends ServiceEntityRepository implements PasswordUpgraderInterface
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, User::class);
    }

    /**
     * Used to upgrade (rehash) the user's password automatically over time.
     */
    public function upgradePassword(PasswordAuthenticatedUserInterface $user, string $newHashedPassword): void
    {
        if (!$user instanceof User) {
            throw new UnsupportedUserException(sprintf('Instances of "%s" are not supported.', \get_class($user)));
        }

        $user->setPassword($newHashedPassword);
        $this->_em->persist($user);
        $this->_em->flush();
    }

    public function findOrCreateFromOauth(string $service, ResourceOwnerInterface $owner, string $avatar=null){
        $user ='';
        switch($service){
            case 'facebook':
                $user = $this->oauthFacebook($owner);
                break;
            case 'github':
                $user = $this->oauthGithub($owner, $avatar);
                break;
            case 'google':
                $user = $this->oauthGoogle($owner);
                break;
        }
        return $user;
    }

    public function oauthFacebook(ResourceOwnerInterface $owner){
        /** @var User|null $user */
        $user = $this->createQueryBuilder('u')
            ->where('u.facebookId = :facebookId')
            ->orWhere('u.email = :email')
            ->setParameters([
                'email' => $owner->getEmail(),
                'facebookId' => $owner->getId()
            ])
            ->getQuery()
            ->getOneOrNullResult();
        if($user){
            if($user->getFacebookId() == null){
                $user->setFacebookId($owner->getId());
                $this->getEntityManager()->flush();
            }
            return $user;
        }
        $user = (new User())
            ->setFacebookId($owner->getId())
            ->setEmail($owner->getEmail())
            ->setAvatar($owner->getPictureUrl())
            ->setPrenom($owner->getFirstName())
            ->setNom($owner->getLastName());

        $em = $this->getEntityManager();
        $em->persist($user);
        $em->flush();

        return $user;
    }

    public function oauthGithub(ResourceOwnerInterface $owner, string $avatar=null){
        /** @var User|null $user */
        $user = $this->createQueryBuilder('u')
            ->where('u.githubId = :githubId')
            ->orWhere('u.email = :email')
            ->setParameters([
                'email' => $owner->getEmail(),
                'githubId' => $owner->getId()
            ])
            ->getQuery()
            ->getOneOrNullResult();
        if($user){
            if($user->getGithubId() == null){
                $user->setGithubId($owner->getId());
                $this->getEntityManager()->flush();
            }
            return $user;
        }
        $user = (new User())
            ->setGithubId($owner->getId())
            ->setEmail($owner->getEmail())
            ->setAvatar($avatar)
            ->setNom($owner->getName());

        $em = $this->getEntityManager();
        $em->persist($user);
        $em->flush();

        return $user;
    }

    public function oauthGoogle(ResourceOwnerInterface $owner){
        /** @var User|null $user */
        $user = $this->createQueryBuilder('u')
            ->where('u.googleId = :googleId')
            ->orWhere('u.email = :email')
            ->setParameters([
                'email' => $owner->getEmail(),
                'googleId' => $owner->getId()
            ])
            ->getQuery()
            ->getOneOrNullResult();
        if($user){
            if($user->getGoogleId() == null){
                $user->setGoogleId($owner->getId());
                $this->getEntityManager()->flush();
            }
            return $user;
        }
        $user = (new User())
            ->setGoogleId($owner->getId())
            ->setEmail($owner->getEmail())
            ->setAvatar($owner->getAvatar())
            ->setPrenom($owner->getFirstName())
            ->setNom($owner->getLastName());

        $em = $this->getEntityManager();
        $em->persist($user);
        $em->flush();

        return $user;
    }

    // /**
    //  * @return User[] Returns an array of User objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('u.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?User
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
