<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Controller\UserPreferencesController;
use App\Repository\UserPreferencesRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

#[ORM\Entity(repositoryClass: UserPreferencesRepository::class)]
#[ApiResource(
    collectionOperations: [
        'get' => [
            'openapi_context' => [
                'security' => [['bearerAuth' => []]]
            ]
        ],
        'post' => [
            'openapi_context' => [
                'security' => [['bearerAuth' => []]]
            ]
        ],
    ],
    iri: 'https://schema.org/Book',
    itemOperations: [
        'get' => [
            'openapi_context' => [
                'security' => [['bearerAuth' => []]]
            ]
        ],
        'put' => [
            'openapi_context' => [
                'security' => [['bearerAuth' => []]]
            ]
        ],
        'patch' => [
            'openapi_context' => [
                'security' => [['bearerAuth' => []]]
            ]
        ],
        'delete' => [
            'openapi_context' => [
                'security' => [['bearerAuth' => []]]
            ]
        ],
    ],
    normalizationContext: ['groups' => ['read:UserPreferences']],
    security: 'is_granted("ROLE_USER")'
)]
class UserPreferences
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    #[Groups(['read:UserPreferences'])]
    private $id;

    #[ORM\Column(type: 'boolean')]
    #[Groups(['read:UserPreferences'])]
    private $sell_opening;

    #[ORM\Column(type: 'boolean')]
    #[Groups(['read:UserPreferences'])]
    private $reservation_received;

    #[ORM\Column(type: 'boolean')]
    #[Groups(['read:UserPreferences'])]
    private $reservation_made;

    /**
     * @ORM\OneToOne(targetEntity=User::class, mappedBy="preferences", cascade={"persist", "remove"})
     * @ORM\JoinColumn(onDelete="CASCADE", nullable=true)
     */
    #[ORM\OneToOne(mappedBy: "preferences", targetEntity: User::class, cascade: ['persist','remove'])]
    #[Groups(['read:UserPreferences'])]
    private $user_id;

    #[ORM\Column(type: 'boolean')]
    #[Groups(['read:UserPreferences'])]
    private $agree_offer;

    #[ORM\Column(type: 'boolean')]
    #[Groups(['read:UserPreferences'])]
    private $receive_offer;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getSellOpening(): ?bool
    {
        return $this->sell_opening;
    }

    public function setSellOpening(bool $sell_opening): self
    {
        $this->sell_opening = $sell_opening;

        return $this;
    }

    public function getReservationReceived(): ?bool
    {
        return $this->reservation_received;
    }

    public function setReservationReceived(bool $reservation_received): self
    {
        $this->reservation_received = $reservation_received;

        return $this;
    }

    public function getReservationMade(): ?bool
    {
        return $this->reservation_made;
    }

    public function setReservationMade(bool $reservation_made): self
    {
        $this->reservation_made = $reservation_made;

        return $this;
    }

    public function getUserId(): ?User
    {
        return $this->user_id;
    }

    public function setUserId(User $user_id): self
    {
        // set the owning side of the relation if necessary
        if ($user_id->getPreference() !== $this) {
            $user_id->setPreference($this);
        }

        $this->user_id = $user_id;

        return $this;
    }

    public function getAgreeOffer(): ?bool
    {
        return $this->agree_offer;
    }

    public function setAgreeOffer(bool $agree_offer): self
    {
        $this->agree_offer = $agree_offer;

        return $this;
    }

    public function getReceiveOffer(): ?bool
    {
        return $this->receive_offer;
    }

    public function setReceiveOffer(bool $receive_offer): self
    {
        $this->receive_offer = $receive_offer;

        return $this;
    }
}