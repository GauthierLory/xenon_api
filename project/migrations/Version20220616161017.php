<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220616161017 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE user ADD github_id VARCHAR(255) DEFAULT NULL, ADD facebook_id VARCHAR(255) DEFAULT NULL, ADD google_id VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE user RENAME INDEX uniq_8d93d649d81022c0 TO UNIQ_8D93D6497CCD6FB7');
        $this->addSql('ALTER TABLE user_preferences ADD user_id_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE user_preferences ADD CONSTRAINT FK_402A6F609D86650F FOREIGN KEY (user_id_id) REFERENCES user (id) ON DELETE CASCADE');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_402A6F609D86650F ON user_preferences (user_id_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE user DROP github_id, DROP facebook_id, DROP google_id');
        $this->addSql('ALTER TABLE user RENAME INDEX uniq_8d93d6497ccd6fb7 TO UNIQ_8D93D649D81022C0');
        $this->addSql('ALTER TABLE user_preferences DROP FOREIGN KEY FK_402A6F609D86650F');
        $this->addSql('DROP INDEX UNIQ_402A6F609D86650F ON user_preferences');
        $this->addSql('ALTER TABLE user_preferences DROP user_id_id');
    }
}
