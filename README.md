# XenonApi


## Installation

Clone the [repo](https://gitlab.com/GauthierLory/xenon_api) to install.

```bash
docker-compose up -d

se connecter au container sf
docker exec -it www_docker_xenonapi bash

cd /project
sudo chown -R $USER ./
```

## Usage

```bash
#adress phpmyadmin
http://127.0.0.1:8080/

# adress maildev
http://127.0.0.1:8081/

# adress web
http://127.0.0.1:8741/

# api
http://127.0.0.1:8741/api

# Launch test postman
newman run ./postman/postman_collection.json -e ./postman/Xenon_api.postman_environment.json

```